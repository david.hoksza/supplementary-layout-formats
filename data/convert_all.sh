#!/bin/bash

MINERVA_INSTANCE=http://localhost:8080/minerva/api
#MINERVA_INSTANCE=https://minerva-dev.lcsb.uni.lu/minerva/api

minerva_token=`curl -X POST -c - --data "login=anonymous&password=" ${MINERVA_INSTANCE}/doLogin | grep MINERVA_AUTH_TOKEN | awk '{print $7}'`

for f in inflamation_cd neuron_cd pdmap_spring18_cd
#for f in pdmap_spring18_cd
#for f in minimalistic
do

	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f}.xml -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/CellDesigner_SBML:SBML > ${f}_to_sbml.xml
	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f}_to_sbml.xml -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/SBML:CellDesigner_SBML > ${f}_to_sbml_to_cd.xml
	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f}.xml -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/CellDesigner_SBML:SBGN-ML > ${f}_to_sbgn.xml
	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f}_to_sbgn.xml -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/SBGN-ML:CellDesigner_SBML > ${f}_to_sbgn_to_cd.xml
		
done

for f in `ls -1 *.xml`
do

	if [[ $f = *"cd."* ]]; then
		input_format=CellDesigner_SBML
	elif [[ $f = *"sbml."* ]]; then
		input_format=SBML
	else
		input_format=SBGN-ML
	fi

	for output_format in svg pdf png
	do
		curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f} -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/image/${input_format}:${output_format} > ${f/.xml/}.${output_format}
		
	done
done
