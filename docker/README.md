# docker compose configuration for <a href='https://minerva.pages.uni.lu/doc/'>minerva platform</a>

## Quickstart guide

To make it running just clone the repository and run docker-compose:

```
git clone https://git-r3lab.uni.lu/minerva/docker.git
cd docker
docker-compose up
```

Please note that to run the conversions on the attached networks, 
the Docker engine will require about 4 GB of RAM (the default size
on Windows is 2 GB).

