\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{pdflscape}
\usepackage{xcolor}

\usepackage[backend=biber,style=authoryear]{biblatex}
\addbibresource{citation.bib}

\title{Supplementary information: Conversion examples with MINERVA conversion API}
\author{david.hoksza@uni.lu}

\begin{document}
\date{}

\maketitle

{\color{red}
All the underlying data for this supplementary information are accessible at \href{https://git-r3lab.uni.lu/david.hoksza/supplementary-layout-formats}{https://git-r3lab.uni.lu/david.hoksza/supplementary-layout-formats}. Please also read section~\ref{sec:reproducibility} if you want to reproduce the results.}

\section{Evaluation set description}

To showcase how MINERVA can convert between CellDesigner SBML (SBML-CD), SBML and SBGN-ML, we utilized Parkinson's disease (PD) map~\autocite{Fujita2014} (available online at \href{https://pdmap.uni.lu}{https://pdmap.uni.lu}) to extract three layout-containing networks with increasing level of complexity.


    \begin{itemize}
        \item Inflammation signalling pathway from the neuron compartment (INF)
            \begin{itemize}
                \item 37 species and 20 reactions
            \end{itemize}
        \item Multi compartment network describing PD-related processes in striatal neurons (SN) 
            \begin{itemize}
                \item 152 species and 132 reactions 
            \end{itemize}
        \item Full Parkinson’s disease map (PDM) 
            \begin{itemize}
                \item about 5,500 species and almost 2500 reactions
            \end{itemize}
    \end{itemize}
    
All the networks contained colors, duplicated nodes, proteins with modified residues and species grouped in complexes, which made them difficult to convert because of different level of support of these features in the three tested formats.
    

Each of the networks was exported from the Parkinson's disease map using MINERVA's export functionality available via the context menu. For the INF and SN, the corresponding regions of the map were selected via \textit{Select mode} $\rightarrow$\textit{ Export as map} $\rightarrow$ \textit{CellDesigner SBML}. The INF and SN files were then adjusted in CellDesigner. Specifically we:

    \begin{itemize}
        \item removed the "Layout annotation" layer. CellDesigner supports layers, visibility of which can be turned on or off in CellDesigner, and MINERVA uses the layers to export pathways layout information (pathways can cross multiple compartments). In order to get rid of information about pathways crossing the exported regions of the map (which are exported together with the required part of the map) and keeping only the compartment information, we removed the "Layout annotation" layer completely for each of the networks.
        \item adjusted the size of the layout to fit the sizes of the exported networks. This was needed because when exporting a region in MINERVA the exported network keeps the original coordinates of the full map resulting in a lot of free space especially if only a small region of a large map is being exported.
    \end{itemize}

The resulting files are attached to this supplementary information.

\subsection{Conversion process}

In order to test validity of the conversion process and to see which features of the networks can be retained and which get lost, we took each of the three networks in SBML-CD and converted it to each of the two remaining formats (SBML and SBGN-ML) and back. In each step we used MINERVA API, first to do the conversion, but also to export the converted files into all three MINERVA-supported graphical formats, i.e., PDF, PNG and SVG. We chose to start the process with CellDesigner SBML, because, with respect to layout and render capabilities, it enables to store super-set of features compared to the remaining two formats.

\subsection{Comment on reproducibility}
\label{sec:reproducibility}

April’18 edition of Parkinson's disease map (accessible at \href{https://webdav-r3lab.uni.lu/public/pdmap/Releases/}{https://webdav-r3lab.uni.lu/public/pdmap/Releases/}) was used to create the INF, SN and PDM networks, which are included in the attached archive. Besides the source networks (in the CellDesigner SBML format), the archive also includes a shell script to carry out the conversions, and target layout files accompanied by image exports from those files. The shell script connects to an instance of MINERVA framework running at \href{https://minerva-dev.lcsb.uni.lu}{https://minerva-dev.lcsb.uni.lu} to carry out the above introduced conversion process.

Please note that the following results were gnerated with MINERVA version 12.2 running at \href{https://minerva-dev.lcsb.uni.lu}{https://minerva-dev.lcsb.uni.lu}. But since MINERVA is being upgraded regularly, it is possible that results obtained in the future might differ from the ones presented here. 

There are two ways to exactly reproduce the results:

    \begin{itemize}
        
        \item Install the respective version of MINERVA available at \href{https://git-r3lab.uni.lu/minerva/core/tree/fdeab47e6803031d363ce33254c8d3a56498f94f}{https://git-r3lab.uni.lu/minerva/core/tree/fdeab47e6803031d363ce33254c8d3a56498f94f} and run the script using that instance as the target one.
        
        \item Use the attached \href{https://www.docker.com/}{Docker} compose script to create a Docker container to compose a Docker image of MINERVA version 12.2. (see README.md in the \textit{docker} directory for details). Then you can access the API of the MINERVA instance running in the built Docker container via the port 8080.
        
    \end{itemize}

\pagebreak
\thispagestyle{empty}
\newgeometry{bottom=3cm}
\begin{landscape}

\subsection{Results}
\subsubsection{SBML-CD $\rightarrow$ SBML $\rightarrow$ SBML-CD}

    \begin{tabular}{l | c  c  c }
        & SDBML-CD & SBML & SBML-CD \\
      \hline			
      INF & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd_to_sbml.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd_to_sbml_to_cd.pdf}  \\
      SN & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd_to_sbml.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd_to_sbml_to_cd.pdf}  \\
      PDM & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd_to_sbml.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd_to_sbml_to_cd.pdf}  \\
    \end{tabular}

\end{landscape}
\restoregeometry

\pagebreak

\thispagestyle{empty}
\newgeometry{bottom=2cm}
\begin{landscape}

\subsubsection{SBML-CD $\rightarrow$ SBGN-ML $\rightarrow$ SBML-CD}

    \begin{tabular}{l | c  c  c }
        & SDBML-CD & SBGN-ML & SBML-CD \\
      \hline			
      INF & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd_to_sbgn.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/inflamation_cd_to_sbgn_to_cd.pdf}  \\
      SN & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd_to_sbgn.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/neuron_cd_to_sbgn_to_cd.pdf}  \\
      PDM & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd_to_sbgn.pdf} & \includegraphics[width=.3\linewidth]{supplementary/img/pdmap_spring18_cd_to_sbgn_to_cd.pdf}  \\
    \end{tabular}

\end{landscape}
\restoregeometry

\pagebreak


\section{Discussion}

\subsection{SBML}

The conversion from SBML-CD to SBML retains all shapes, colors, positions and compartments. The labels texts are preserved, but in case of compartments their position differs in the SBML version in all the networks. However, this does not impact perception of the networks.

Conversion from SBML back to SBML-CD retains all content fully.

\subsection{SBGN-ML}

The biggest issue when converting from SBML to SBGN-ML is that some reactions get lost because the starting networks contain processes of different levels of detail and therefore one would need to mix SBGN PD and AF languages to express the networks in SBGN-ML. This would, however, result in an invalid SBGN-ML document (see the main paper for discussion of this issue). Moreover, colors are not preserved, because SBGN-ML natively does not support any rendering information. Positions and shapes of entities are retained correctly.

Conversion from SBGN-ML back to SBML-CD retains all content fully.

\printbibliography 
\end{document}